vim-status
==========
A lightweight statusline and tabline for vim and neovim

- Colours, separators, and layout are easily configurable
- Small and hackable base, just a single .vim file (excluding themes)
- Powerline style separators and filetype icons (*with supporting font*)
- Integrated with with other plugins, ale, fugitive, (*possibly more*..)

Screenshots
---

Both the default and minimal use the current theme gui colors if available and the terminal 0-7 colors.

[![default.png](https://cdn.scrot.moe/images/2019/02/16/default.png)](https://scrot.moe/image/aDGNu)

[![minimal.png](https://cdn.scrot.moe/images/2019/02/16/minimal.png)](https://scrot.moe/image/aDeZz)


Some themes I've made for myself over the last year to accommodate my setup

[![jinx.png](https://cdn.scrot.moe/images/2019/02/16/jinx.png)](https://scrot.moe/image/aDzDe)

[![midnight.png](https://cdn.scrot.moe/images/2019/02/16/midnight.png)](https://scrot.moe/image/aDw8Q)

[![material.png](https://cdn.scrot.moe/images/2019/02/16/material.png)](https://scrot.moe/image/aDKPn)

---
Installation
------------
##### vim-plug

```
Plug 'https://bitbucket.org/natemaia/vim-status'
```

##### pathogen

```
git clone https://bitbucket.org/natemaia/vim-status ~/.vim/bundle/vim-status
```

##### vim8

```
mkdir -p ~/.vim/pack/plugins/start
git clone https://bitbucket.org/natemaia/vim-status ~/.vim/pack/plugins/start/vim-status
```

---
Usage
---

Change the theme
```
:StatusTheme <Tab>
```

Refresh the statusline
```
:StatusRefresh
```

---
Configuration
---

In order to configure the statusline it is generally recommended to create a theme file and
place it somewhere on the `runtimepath` under `themes/your_theme.vim` and configure to your
liking, you can use the `default` theme as a template to understand the settings.
If you just want to tweak a few values in an existing theme, you're able to do so by setting
any of the below variables in your vimrc or init.vim

---

To define a theme other than the default
```
let g:status_theme = 'theme_name'
```

To enable/disable configurable shown items
```
let g:status_show_mode            = 1
let g:status_show_filesize        = 0
let g:status_show_linter          = 0
let g:status_show_filetype_icons  = 0
```

To enable/disable colorizing of fields
```
let g:status_colorize_mode     = 1
let g:status_colorize_paste    = 1
let g:status_colorize_linter   = 1
let g:status_colorize_linenum  = 0
let g:status_colorize_filesize = 0
let g:status_colorize_modified = 1
let g:status_colorize_filetype = 1
```


To change the default icons/strings used
```
let g:status_left_sep          = ''
let g:status_right_sep         = ''
let g:status_modified_char     = '+'
let g:status_paste_char        = 'P'
let g:status_readonly_char     = 'RO'
let g:status_tab_close_char    = 'X'
```


To add a missing filetype icon to the dict
```
let g:status_filetypes['filetype'] = ':)'
```



To change the string used for every mode
```
let g:status_modes = {
	\ 'n' : 'N',   'i' : 'I',    'R' :     'R',
	\ 'v' : 'V',   'V' : 'V',    "\<C-v>": 'V',
	\ 's' : 'S',   'S' : 'S',    "\<C-s>": 'S',
	\ 'c' : 'C',   'cv': 'EX',   'ce':     'EX',
	\ 'r' : 'P',   '!' : 'SH',   't' :     'T',
	\ 'rm': 'M',   'r?': 'CF',   'Rv':     'VR'
	\ }
```


To only change a single mode, eg. for normal mode
```
let g:status_modes['n'] = 'Normal'
```


To change the colours used, the fist is true color, the second is 256 color
```
let g:status_colors = {
	\ 'backgr' : ['#4D5057', 0],
	\ 'red'    : ['#F2777A', 1],
	\ 'green'  : ['#8DBC8D', 2],
	\ 'yellow' : ['#CE9D00', 3],
	\ 'blue'   : ['#6699CC', 4],
	\ 'purple' : ['#CC99CC', 5],
	\ 'cyan'   : ['#5DD5FF', 6],
	\ 'foregr' : ['#999999', 7],
	\ 'orange' : ['#F99157', 10],
	\ }
```


To change the highlight groups Note these need to use colors from `g:status_colors` to support both 256 and true colors
The defaults also don't use `StatusLine` or `StatusLineNC` to avoid messing with the completion menu and wildmenu colors
```
let g:status_color_groups = {
	\ 'Status'          : ['backgr', 'foregr',     ''],
	\ 'StatusAlt'       : ['foregr', 'backgr',     ''],
	\ 'TabLine'         : ['foregr', 'backgr',     ''],
	\ 'TabLineFill'     : ['backgr', 'backgr',     ''],
	\ 'TabLineSel'      : ['green',  'backgr',     ''],
	\ 'TabLineClose'    : ['red',    'backgr',     ''],
	\ 'TabLineSelAlt'   : ['backgr', 'green',  'bold'],
	\ 'TabLineCloseAlt' : ['backgr', 'red',    'bold'],
	\ 'StatusInsert'    : ['cyan',   'backgr',     ''],
	\ 'StatusNormal'    : ['green',  'backgr',     ''],
	\ 'StatusVisual'    : ['purple', 'backgr',     ''],
	\ 'StatusReplace'   : ['red',    'backgr',     ''],
	\ 'StatusSelect'    : ['yellow', 'backgr',     ''],
	\ 'StatusInsertAlt' : ['backgr', 'cyan',   'bold'],
	\ 'StatusNormalAlt' : ['backgr', 'green',  'bold'],
	\ 'StatusVisualAlt' : ['backgr', 'purple', 'bold'],
	\ 'StatusReplaceAlt': ['backgr', 'red',    'bold'],
	\ 'StatusSelectAlt' : ['backgr', 'yellow', 'bold'],
	\ }
```


To add a new one without changing the defaults you can define it in your vimrc
```
let g:status_basic_filetypes['match'] = 'replacement'
```

To change the colourgroups used when switching modes using groups defined in `g:status_color_groups`
As explained above, the defaults don't use `StatusLine` or `StatusLineNC` to avoid changing colors
possibly set by the user for the completion menu and wildmenu.
```
let g:status_base_color        = 'Status'
let g:status_normal_color      = 'StatusNormal'
let g:status_insert_color      = 'StatusInsert'
let g:status_visual_color      = 'StatusVisual'
let g:status_select_color      = 'StatusSelect'
let g:status_replace_color     = 'StatusReplace'
let g:status_alt_base_color    = 'StatusAlt'
let g:status_alt_normal_color  = 'StatusNormalAlt'
let g:status_alt_insert_color  = 'StatusInsertAlt'
let g:status_alt_visual_color  = 'StatusVisualAlt'
let g:status_alt_select_color  = 'StatusSelectAlt'
let g:status_alt_replace_color = 'StatusReplaceAlt'
let g:status_git_char_color    = 'GitStatus'

let g:status_tab_base          = 'TabLine'
let g:status_tab_select        = 'TabLineSel'
let g:status_tab_close         = 'TabLineClose'
let g:status_alt_tab_base      = 'TabLineFill'
let g:status_alt_tab_select    = 'TabLineSelAlt'
let g:status_alt_tab_close     = 'TabLineCloseAlt'
```

Some filetypes or buffers we don't want to be treated the same as others (vim-plug, quickfix, etc..),
for these we use the simplified "alt" statusline and can set the title as we please.
Below are the defaults matched currently.

```
let g:status_basic_filetypes = {
	\ 'vim-plug'          : ' vim-plug ',
	\ 'ranger'            : ' ranger ',
	\ 'nnn'               : ' nnn ',
	\ 'gundo'             : ' gundo ',
	\ 'undotree'          : ' undotree ',
	\ 'term'              : ' '.fnamemodify($SHELL,':t:r').' ',
	\ 'vimv'              : ' vimv ',
	\ 'diff'              : ' diff ',
	\ 'unite'             : ' unite ',
	\ 'vimfiler'          : ' vimfiler ',
	\ 'vimshell'          : ' vimshell ',
	\ '__Gundo_Preview__' : ' gundo preview ',
	\ 'minibuffexpl'      : ' minibuffexpl ',
	\ }

```
