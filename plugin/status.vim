" ---------------------------------------------------- "
" vim-status -> A lightweight statusline and tabline "
"     Plugin written by Nathaniel Maia, March 2018     "
" ---------------------------------------------------- "

scriptencoding utf8
if exists('g:vim_status_loaded') || !has('statusline')
	finish
endif
let g:vim_status_loaded = 1

function! Status(win) " {{{1
	let l:mode    = ''
	let l:color   = ''
	let l:active  = a:win == winnr()
	let l:buffer  = winbufnr(a:win)
	let l:bufnums = "%{winnr('$')>1?'['.".a:win.".'/'.winnr('$').(winnr('#')==".a:win."?'#':'').']':''}"

	" wrap the statusline setup in a try-catch to avoid
	" throwing mass errors and making the editor unusable
	try
		" inactive or special buffer status line
		let l:status = AltStatus(l:active, l:buffer, l:bufnums)
		if l:status !=? ''
			return l:status
		endif
		" create a statusline for the given window (win)
		let l:status .= s:alt_base_color
		let l:status .= CurrentMode(l:active, s:left_sep, l:bufnums)
		let l:status .= s:alt_base_color
		let l:status .= BufferName(l:active, l:buffer)
		let l:status .= ReadOnly(l:active, l:buffer)
		let l:status .= SetPaste(l:active)
		let l:status .= s:alt_base_color.'%='
		let l:status .= Filetype(l:active, l:buffer)
		let l:status .= s:module_separator
		let l:status .= FileSize(l:active, s:right_sep)
		let l:status .= s:module_separator
		let l:status .= LineNum(l:active, s:right_sep, l:buffer)
		let l:status .= s:module_separator
		let l:status .= LintStat(l:active, s:right_sep)
		return l:status
	catch
		echomsg 'an error occurred while initializing the statusline for this window'
		set laststatus=0
	endtry
endfunction

function! AltStatus(active, buf, bufnums) abort " {{{1
	let l:name = bufname(a:buf)
	let l:type = getbufvar(a:buf, '&buftype')
	let l:ft   = getbufvar(a:buf, '&filetype')
	let l:stat = l:type ==? 'help' ? ' help: '.fnamemodify(l:name, ':t:r').' '
				\ : l:name =~? 'term://' ? s:basic_filetypes['term']
				\ : l:name =~? '/vimv.'  ? s:basic_filetypes['vimv'].$PWD.' '
				\ : has_key(s:basic_filetypes, l:ft)   ? s:basic_filetypes[l:ft]
				\ : has_key(s:basic_filetypes, l:name) ? s:basic_filetypes[l:name]
				\ : ''
	if l:stat !=# ''
		let l:stat  = CurrentMode(a:active, s:left_sep, a:bufnums).l:stat.'%='
		let l:stat .= LineNum(a:active, s:right_sep, a:buf)
		return l:stat
	endif
	return ''
endfunction

function! TabLine() " {{{1
	let l:line = ''
	for l:tab in range(tabpagenr('$'))
		let l:cur_tab = l:tab + 1
		let l:line .= l:cur_tab == tabpagenr() ? s:alt_tab_select : s:alt_tab_base
		let l:line .= (l:cur_tab != 1 && l:cur_tab == tabpagenr()) ? s:left_sep : ''
		let l:line .= ' ['.'%'.l:cur_tab.'T'.l:cur_tab.']'
		let l:name  = ''
		let l:mod   = 0
		for l:buf in tabpagebuflist(l:tab + 1)
			let l:btype = getbufvar(l:buf, '&buftype')
			if l:btype ==? '\v(help|quickfix)'
				let l:name .= l:btype ==? 'help' ? '[H]'.fnamemodify(bufname(l:buf), ':t:s/.txt$//') : '[Q]'
			elseif getbufvar(l:buf, '&modifiable')
				let l:name .= fnamemodify(bufname(l:buf), ':t').', '
			endif
			let l:mod += getbufvar(l:buf, '&modified') ? 1 : 0
		endfor
		let l:line .= l:mod > 0 ? ' ' : ' '
		let l:name  = substitute(l:name, '^,', '[New],', '')
		let l:name  = substitute(l:name, ', ,', ', [New],', '')
		let l:name  = substitute(l:name, ', $', '', '')
		let l:line .= l:name ==# '' ? '[New] ' : l:name.' '
		let l:line .= l:cur_tab == tabpagenr() ? s:tab_select.s:left_sep : ' '
	endfor
	let l:line .= s:alt_tab_base.' %T'
	if tabpagenr('$') > 1
		let l:line .= '%='.s:tab_close.s:right_sep.s:alt_tab_close.'%999X '.s:tab_close_char.' '
	endif
	return l:line
endfunction

function! CurrentMode(active, sepr, bufnums) abort " {{{1
	if !s:show_mode
		return a:bufnums
	endif
	let l:mode = mode()
	let l:name = s:modes[l:mode]
	let l:len  = len(l:name) + 1
	if a:active && s:colorize_mode
		let l:item = a:bufnums.' %-'.l:len.'('.l:name.'%)'
		if l:mode ==# 'n'
			let g:mode_color     = s:normal_color
			let g:mode_alt_color = s:alt_normal_color
		elseif l:mode ==# 'i'
			let g:mode_color     = s:insert_color
			let g:mode_alt_color = s:alt_insert_color
		elseif l:mode =~? '\v(v|V||t)'
			let g:mode_color     = s:visual_color
			let g:mode_alt_color = s:alt_visual_color
		elseif l:mode =~? '\v(s|S|)'
			let g:mode_color     = s:select_color
			let g:mode_alt_color = s:alt_select_color
		else
			let g:mode_color     = s:replace_color
			let g:mode_alt_color = s:alt_replace_color
		endif
		let l:item = g:mode_alt_color.l:item.g:mode_color.a:sepr.s:alt_base_color
	else
		let l:item = a:bufnums.' '.s:alt_base_color
	endif
	return l:item
endfunction

function! FileSize(active, sepr) abort " {{{1
	if s:show_filesize && a:active
		let l:b = getfsize(expand('%:p'))
		let l:b = l:b >= 1    ? l:b        : '0'
		let l:k = l:b >= 1000 ? l:b / 1000 : ''
		let l:m = l:k >= 1000 ? l:k / 1000 : ''
		let l:r = l:m !=? '' ? ' '.l:m.'M ' : l:k !=? '' ? ' '.l:k.'K ' : ' '.l:b.'B '
		if s:colorize_filesize
			return s:insert_color.a:sepr.s:alt_insert_color.l:r.a:sepr.s:alt_base_color
		endif
		return l:r
	endif
	return ''
endfunction

function! LintStat(active, sepr) abort " {{{1
	if a:active && s:show_linter
		try " why is ale#statusline#Count not exposed to exists()
			let l:num = ale#statusline#Count(bufnr(''))
		catch
			return ''
		endtry
		let l:err = l:num.error + l:num.style_error
		let l:wrn = l:num.total - l:err
		let l:tot = l:num.total == 0 ? '' : printf(' %dW %dE ', l:wrn, l:err)
		if s:colorize_linter && l:tot !=? ''
			return s:select_color.a:sepr.s:alt_select_color.l:tot
		endif
		return l:tot
	endif
	return ''
endfunction

function! ReadOnly(active, buffer) abort " {{{1
	if getbufvar(a:buffer, '&readonly') || !getbufvar(a:buffer, '&modifiable')
		if s:colorize_paste && a:active
			return s:replace_color.' '.s:readonly_char.' '.s:alt_base_color
		else
			return ' '.s:readonly_char.' '
		endif
	else
		return ''
	endif
endfunction

function! Filetype(active, buffer) abort " {{{1
	if !a:active
		return ''
	endif
	let l:bt = getbufvar(a:buffer, '&filetype')
	let l:ft = l:bt !=? '' ? l:bt : 'none'
	if s:show_filetype_icons && !empty(s:filetypes)
		let l:tp = has_key(s:filetypes, l:bt) ? s:filetypes[l:bt] : ''
		if s:colorize_filetype
			return s:normal_color.l:tp.' '.s:alt_base_color.l:ft.' '
		endif
		return l:tp.' '.l:ft.' '
	endif
	return l:ft.' '
endfunction

function! LineNum(active, sepr, buffer) abort " {{{1
	let l:c  = line('.')
	let l:l  = line('$')
	let l:cl = col('.')
	let l:cl = (l:cl > 99) ? ' %3v ' : ' %2v '
	let l:ln = (l:l > 999) ? '%3l' : '%2l'
	let l:pc = (l:c < 2) ? ' TOP ' : (l:c == l:l) ? ' BOT ' : '%3p%% '
	let l:r  = ' Ln '.l:ln.': Col'.l:cl.l:pc

	if a:active && s:colorize_linenum
		return s:normal_color.a:sepr.s:alt_normal_color.' %'.l:r.a:sepr.s:alt_base_color
	elseif a:active
		return l:r
	endif
	return l:r.' '
endfunction

function! Modified(active, buffer) abort " {{{1
	if !getbufvar(a:buffer, '&modified')
		return ''
	elseif a:active && s:colorize_modified
		return s:normal_color.' '.s:modified_char.s:alt_base_color
	else
		return ' '.s:modified_char
	endif
endfunction

function! SetPaste(active) abort " {{{1
	if &paste
		if s:colorize_paste && a:active
			return s:replace_color.' '.s:paste_char.' '.s:alt_base_color
		else
			return ' '.s:paste_char.' '
		endif
	else
		return ''
	endif
endfunction

function! BufferName(active, buffer) abort " {{{1
	return Modified(a:active, a:buffer).' %<%f '
endfunction

function! WidthPart(str, width) abort  " {{{1
	if a:width <= 0
		return ''
	endif
	let l:ret   = a:str
	let l:width = strwidth(a:str)
	while l:width > a:width
		let l:char   = matchstr(l:ret, '.$')
		let l:ret    = l:ret[: -1 - len(l:char)]
		let l:width -= strwidth(l:char)
	endwhile
	return l:ret
endfunction

function! <SID>SetTheme(theme) abort " {{{1
	if empty(a:theme)
		return ''
	else
		let l:theme = globpath(&runtimepath, 'themes/'.a:theme.'*')
		if empty(l:theme)
			echo 'Failed to find theme path for '.a:theme
			return ''
		endif
		let g:status_theme = a:theme
		let g:colors_name = a:theme
	endif

	try
		execute 'silent source '.l:theme
	catch
		echo 'Failed to source theme: '.a:theme
		let g:status_theme = ''
		let g:colors_name = ''
		return ''
	endtry
	call <SID>Defaults()

	for l:group in keys(s:color_groups)
		let l:foregr = s:color_groups[l:group][0]
		let l:backgr = s:color_groups[l:group][1]
		let l:attr   = s:color_groups[l:group][2]
		if !empty(l:foregr)
			let l:fg_color = get(s:colors, l:foregr)
			let l:fg_true  = empty(l:fg_color[0]) ? '' : ' guifg='.l:fg_color[0]
			let l:fg_term  = empty(l:fg_color[1]) ? '' : ' ctermfg='.l:fg_color[1]
			if l:fg_term != '' || l:fg_true != ''
				execute 'silent highlight! '.l:group.l:fg_true.l:fg_term
			endif
		endif
		if !empty(l:backgr)
			let l:bg_color = get(s:colors, l:backgr)
			let l:bg_true  = empty(l:bg_color[0]) ? '' : ' guibg='.l:bg_color[0]
			let l:bg_term  = empty(l:bg_color[1]) ? '' : ' ctermbg='.l:bg_color[1]
			if l:bg_term != '' || l:bg_true != ''
				execute 'silent highlight! '.l:group.l:bg_true.l:bg_term
			endif
		endif
		if !empty(l:attr)
			execute 'silent highlight! '.l:group.' gui='.l:attr.' cterm='.l:attr
		endif
	endfor

	call <SID>RefreshStatus()
endfunction

function! <SID>Defaults() abort " {{{1
	" for each value if one isn't defined by the theme,
	" then the default will be used

	let l:default_colorgroups = {
				\ 'base_color'       : 'Status',           'normal_color'    : 'StatusNormal',
				\ 'insert_color'     : 'StatusInsert',     'visual_color'    : 'StatusVisual',
				\ 'replace_color'    : 'StatusReplace',    'select_color'    : 'StatusSelect',
				\ 'alt_base_color'   : 'StatusAlt',        'alt_normal_color': 'StatusNormalAlt',
				\ 'alt_insert_color' : 'StatusInsertAlt',  'alt_visual_color': 'StatusVisualAlt',
				\ 'alt_replace_color': 'StatusReplaceAlt', 'alt_select_color': 'StatusSelectAlt',
				\ 'tab_base'         : 'Status',           'alt_tab_base'    : 'StatusAlt',
				\ 'tab_select'       : 'StatusNormal',     'alt_tab_select'  : 'StatusNormalAlt',
				\ 'tab_close'        : 'StatusReplace',    'alt_tab_close'   : 'StatusReplaceAlt',
				\ }

	let l:default_options = {
				\ 'colors': {
				\ 'backgr' : ['#4D5057', 0],
				\ 'red'    : ['#F2777A', 1],
				\ 'green'  : ['#8DBC8D', 2],
				\ 'yellow' : ['#CE9D00', 3],
				\ 'blue'   : ['#6699CC', 4],
				\ 'purple' : ['#CC99CC', 5],
				\ 'cyan'   : ['#5DD5FF', 6],
				\ 'foregr' : ['#999999', 7],
				\ 'orange' : ['#F99157', 10],
				\ },
				\ 'modes': {
				\ 'n' : 'N', 'i' : 'I',  'R' : 'R',
				\ 'v' : 'V', 'V' : 'V',  '': 'V',
				\ 's' : 'S', 'S' : 'S',  '': 'S',
				\ 'c' : 'C', 'cv': 'EX', 'ce': 'EX',
				\ 'r' : 'P', '!' : 'SH', 't' : 'T',
				\ 'rm': 'M', 'r?': 'CF', 'Rv': 'VR'
				\ },
				\ 'basic_filetypes': {
				\ 'netrw'             : ' netrw ',
				\ 'vim-plug'          : ' vim-plug ',
				\ 'ranger'            : ' ranger ',
				\ 'nnn'               : ' nnn ',
				\ 'noice'             : ' noice ',
				\ 'gundo'             : ' gundo ',
				\ 'undotree'          : ' undotree ',
				\ 'term'              : ' '.fnamemodify($SHELL,':t:r').' ',
				\ 'vimv'              : ' vimv ',
				\ 'diff'              : ' diff ',
				\ 'unite'             : ' unite ',
				\ 'vimfiler'          : ' vimfiler ',
				\ 'vimshell'          : ' vimshell ',
				\ '__Gundo_Preview__' : ' gundo preview ',
				\ 'minibuffexpl'      : ' minibuffexpl ',
				\ },
				\ 'filetypes': {},
				\ 'color_groups': {
				\ 'Status'           : ['backgr', 'foregr',     ''],
				\ 'StatusInsert'     : ['cyan',   'backgr',     ''],
				\ 'StatusNormal'     : ['green',  'backgr',     ''],
				\ 'StatusVisual'     : ['purple', 'backgr',     ''],
				\ 'StatusReplace'    : ['red',    'backgr',     ''],
				\ 'StatusSelect'     : ['yellow', 'backgr',     ''],
				\ 'StatusAlt'        : ['foregr', 'backgr',     ''],
				\ 'StatusInsertAlt'  : ['backgr', 'cyan',   'bold'],
				\ 'StatusNormalAlt'  : ['backgr', 'green',  'bold'],
				\ 'StatusVisualAlt'  : ['backgr', 'purple', 'bold'],
				\ 'StatusReplaceAlt' : ['backgr', 'red',    'bold'],
				\ 'StatusSelectAlt'  : ['backgr', 'yellow', 'bold'],
				\ },
				\ 'tab_close_char'   : 'X',  'modified_char'      : '+',
				\ 'readonly_char'    : 'RO', 'paste_char'         : 'P',
				\ 'right_sep'        : '',   'left_sep'           : '',
				\ 'show_mode'        : 1,    'show_filesize'      : 0,
				\ 'show_linter'      : 0,    'show_filetype_icons': 0,
				\ 'colorize_linter'  : 0,    'colorize_filesize'  : 0,
				\ 'colorize_linenum' : 0,    'colorize_filetype'  : 1,
				\ 'colorize_modified': 1,    'colorize_mode'      : 1,
				\ 'colorize_paste'   : 1,    'module_separator'   : '',
				\ }

	for l:group in keys(l:default_colorgroups)
		let s:{l:group} = exists('g:status_'.l:group) ? '%#'.g:status_{l:group}.'#' : '%#'.l:default_colorgroups[l:group].'#'
	endfor
	for l:option in keys(l:default_options)
		let s:{l:option} = exists('g:status_'.l:option) ? g:status_{l:option} : l:default_options[l:option]
	endfor
endfunction

function! <SID>StatusThemes(arglead, cmdline, cursorpos) abort " {{{1
	return map(split(globpath(&runtimepath, 'themes/'.a:arglead.'*'), "\n"), 'fnamemodify(v:val, ":t:r")')
endfunction

function! <SID>RefreshStatus() abort " {{{1
	for l:win in range(1, winnr('$'))
		silent! call setwinvar(l:win, '&statusline', '%!Status('.l:win.')')
	endfor
endfunction

" Setup {{{1

" set minimal theme for terminals without 256 color support and default theme when g:status_theme is unset or empty
let g:status_theme = (!has('gui_running') && &t_Co < 256) ? 'minimal'
			\ : (!exists('g:status_theme') || empty(g:status_theme)) ? 'default'
			\ : g:status_theme

call <SID>SetTheme(g:status_theme)

set showtabline=1
set laststatus=2
set title
set titlelen=95
set tabline=%!TabLine()

command! -bar -nargs=? -complete=customlist,<SID>StatusThemes StatusTheme call <SID>SetTheme(<f-args>)
command! StatusRefresh call <SID>RefreshStatus() | echo 'Status refreshed!'

" title
let s:vimname      = has('nvim') ? 'Neovim' : 'Vim'
let s:twidth       = 'WidthPart(fnamemodify(getcwd(), ":~"), &columns-len(expand("%:p:.:~")))'
let &g:titlestring = s:vimname.' - %{expand("%:p:~:.")}%(%m%r%w%)%<'

augroup StatusRefreshAutocmd
	autocmd!
	autocmd VimEnter,WinEnter,BufWinEnter * call <SID>RefreshStatus()
	autocmd ColorScheme * call <SID>SetTheme(g:status_theme) | call <SID>RefreshStatus()
augroup END

" vim:fdm=marker
