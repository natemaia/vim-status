" simpl-status default theme

" this default theme serves as a base for others to read
" all fields have been filled out even if in their default values

scriptencoding utf8

" bail when no color support or this is NOT the theme defined by g:status_theme
if (!has('gui_running') && &t_Co != 256) || (!exists('g:status_theme')
            \ || g:status_theme !=? fnamemodify(expand('<sfile>'), ':t:r'))
    finish
endif

" 1 is enabled, 0 is disabled
let g:status_show_mode            = 1
let g:status_show_filesize        = 0
let g:status_show_linter          = 0
let g:status_show_filetype_icons  = 0
let g:status_colorize_paste       = 1
let g:status_colorize_mode        = 1
let g:status_colorize_filesize    = 0
let g:status_colorize_modified    = 1
let g:status_colorize_filetype    = 1
let g:status_colorize_linenum     = 0
let g:status_colorize_linter      = 0

" prefixes / symbols for settings
let g:module_separator         = '|'
let g:status_left_sep          = ''
let g:status_right_sep         = ''
let g:status_readonly_char     = 'RO'
let g:status_modified_char     = '+'
let g:status_paste_char        = 'P'
let g:status_tab_close_char    = 'X'

" color groups used for mode switching
" alt_* groups should have colours reversed
let g:status_base_color        = 'Status'
let g:status_normal_color      = 'StatusNormal'
let g:status_insert_color      = 'StatusInsert'
let g:status_visual_color      = 'StatusVisual'
let g:status_select_color      = 'StatusSelect'
let g:status_replace_color     = 'StatusReplace'
let g:status_alt_base_color    = 'StatusAlt'
let g:status_alt_normal_color  = 'StatusNormalAlt'
let g:status_alt_insert_color  = 'StatusInsertAlt'
let g:status_alt_visual_color  = 'StatusVisualAlt'
let g:status_alt_select_color  = 'StatusSelectAlt'
let g:status_alt_replace_color = 'StatusReplaceAlt'

let g:status_tab_base          = 'Status'
let g:status_tab_select        = 'StatusNormal'
let g:status_tab_close         = 'StatusReplace'
let g:status_alt_tab_base      = 'StatusAlt'
let g:status_alt_tab_select    = 'StatusNormalAlt'
let g:status_alt_tab_close     = 'StatusReplaceAlt'


" hack to get the current colorscheme foreground and background colors
let s:truefgr = synIDattr(synIDtrans(hlID('Normal')), 'fg', 'gui')
let s:truebgr = synIDattr(synIDtrans(hlID('Normal')), 'bg', 'gui')
let s:truefgr = empty('s:truefgr') ? '#E1E1E1' : s:truefgr
let s:truebgr = empty('s:truebgr') ? '#111111' : s:truebgr

" the following two dictionaries will looped over by a highlight function this
" will map each color and attribute to the proper gui*= and cterm*= , the second
" dictionary contains the mappings for what highlight groups to create or set

" color dictionary
"
" dict format: name  :  [ truecolor, 256color ]
let g:status_colors = {
            \ 'backgr' : [s:truebgr, 0],
            \ 'red'    : ['#F2777A', 1],
            \ 'green'  : ['#8DBC8D', 2],
            \ 'yellow' : ['#CE9D00', 3],
            \ 'blue'   : ['#6699CC', 4],
            \ 'purple' : ['#CC99CC', 5],
            \ 'cyan'   : ['#5DD5FF', 6],
            \ 'foregr' : [s:truefgr, 7],
            \ 'orange' : ['#F99157', 10],
            \ }

" highlight group map dictionary with colours from above
"
" dict format: highlight group : [ foreground, background, attribute ]
let g:status_color_groups = {
            \ 'Status'          : ['backgr',    'foregr',        ''],
            \ 'StatusInsert'    : ['cyan',      'backgr',        ''],
            \ 'StatusNormal'    : ['green',     'backgr',        ''],
            \ 'StatusVisual'    : ['purple',    'backgr',        ''],
            \ 'StatusReplace'   : ['red',       'backgr',        ''],
            \ 'StatusSelect'    : ['yellow',    'backgr',        ''],
            \ 'StatusAlt'       : ['foregr',    'backgr',        ''],
            \ 'StatusInsertAlt' : ['backgr',    'cyan',      'bold'],
            \ 'StatusNormalAlt' : ['backgr',    'green',     'bold'],
            \ 'StatusVisualAlt' : ['backgr',    'purple',    'bold'],
            \ 'StatusReplaceAlt': ['backgr',    'red',       'bold'],
            \ 'StatusSelectAlt' : ['backgr',    'yellow',    'bold'],
            \ }


" icons to show which filetype is currently being edited
let g:status_filetypes = {}
