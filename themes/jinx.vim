" simpl-status jinx theme

scriptencoding utf8

" bail when no color support or this is NOT the theme defined by g:status_theme
if (!has('gui_running') && &t_Co != 256) || (!exists('g:status_theme')
            \ || g:status_theme !=? fnamemodify(expand('<sfile>'), ':t:r'))
    finish
endif

" 1 is enabled, 0 is disabled
let g:status_show_linter         = 1
let g:status_colorize_linter     = 1
let g:status_show_filetype_icons = 1

" prefixes / symbols for settings       
let g:status_left_sep       = ''
let g:status_right_sep      = ''
let g:status_readonly_char  = ''
let g:status_modified_char  = ''
let g:status_paste_char     = ''
let g:status_tab_close_char = ''

" dict format: name  :  [ truecolor, 256color ]
let g:status_colors = {
            \ 'backgr' : ['#4D5057', 0],
            \ 'red'    : ['#F2777A', 1],
            \ 'green'  : ['#8DBC8D', 2],
            \ 'yellow' : ['#CE9D00', 3],
            \ 'blue'   : ['#6699CC', 4],
            \ 'purple' : ['#CC99CC', 5],
            \ 'cyan'   : ['#5DD5FF', 6],
            \ 'foregr' : ['#999999', 7],
            \ 'orange' : ['#F99157', 10],
            \ }

let g:status_filetypes = {
            \ 'sh'        : '', 'zsh'     : '', 'vim'      : '',
            \ 'python'    : '', 'c'       : '', 'cpp'      : '',
            \ 'markdown'  : '', 'html'    : '', 'xml'      : '',
            \ 'javascript': '', 'clojure' : '', 'go'       : '',
            \ 'scala'     : '', 'sass'    : '', 'lua'      : '',
            \ 'haskell'   : '', 'diff'    : '', 'h'        : '',
            \ 'css'       : '', 'ruby'    : '', 'php'      : '',
            \ 'perl'      : '', 'rust'    : '', 'netrw'    : '',
            \ 'config'    : '', 'ini'     : '', 'gitcommit': '',
            \ 'xdefaults' : '', 'xf86conf': '', 'vim-plug' : '',
            \ 'tmux'      : '', 'cfg'     : '', 'ranger'   : '',
            \ }
