" simpl-status jinx theme

scriptencoding utf8

" bail when no color support or this is NOT the theme defined by g:status_theme
if (!has('gui_running') && &t_Co != 256) || (!exists('g:status_theme')
            \ || g:status_theme !=? fnamemodify(expand('<sfile>'), ':t:r'))
    finish
endif

" 1 is enabled, 0 is disabled
let g:status_show_linter         = 1
let g:status_show_filesize       = 1
let g:status_show_filetype_icons = 1
let g:status_colorize_linter     = 1

" prefixes / symbols for settings       
let g:status_left_sep       = ''
let g:status_right_sep      = ''
let g:status_readonly_char  = ''
let g:status_modified_char  = ''
let g:status_paste_char     = ''
let g:status_tab_close_char = ''

" dict format: name  :  [ truecolor, 256color ]
let g:status_colors = {
            \ 'backgr' : ['#263238', 0],
            \ 'red'    : ['#EE5555', 1],
            \ 'green'  : ['#88BB88', 2],
            \ 'yellow' : ['#FFCC66', 3],
            \ 'blue'   : ['#4488CC', 4],
            \ 'purple' : ['#AA88CC', 5],
            \ 'cyan'   : ['#55CCFF', 6],
            \ 'foregr' : ['#E1E1E1', 7],
            \ 'orange' : ['#EE8844', 10],
            \ }

let g:status_filetypes = {
            \ 'sh'        : '', 'zsh'     : '', 'vim'      : '',
            \ 'python'    : '', 'c'       : '', 'cpp'      : '',
            \ 'markdown'  : '', 'html'    : '', 'xml'      : '',
            \ 'javascript': '', 'clojure' : '', 'go'       : '',
            \ 'scala'     : '', 'sass'    : '', 'lua'      : '',
            \ 'haskell'   : '', 'diff'    : '', 'h'        : '',
            \ 'css'       : '', 'ruby'    : '', 'php'      : '',
            \ 'perl'      : '', 'rust'    : '', 'netrw'    : '',
            \ 'config'    : '', 'ini'     : '', 'gitcommit': '',
            \ 'xdefaults' : '', 'xf86conf': '', 'vim-plug' : '',
            \ 'tmux'      : '', 'cfg'     : '', 'ranger'   : '',
            \ }
