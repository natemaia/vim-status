" simpl-status minimal theme

scriptencoding utf8

" bail when not the set theme
if (!exists('g:status_theme') || g:status_theme !=? fnamemodify(expand('<sfile>'), ':t:r'))
    finish
endif

" 1 is enabled, 0 is disabled
let g:status_colorize_paste    = 0
let g:status_colorize_mode     = 0
let g:status_colorize_filesize = 0
let g:status_colorize_modified = 0
let g:status_colorize_filetype = 0

" hack to get the current colorscheme foreground and background colors
let s:truefgr = synIDattr(synIDtrans(hlID('Normal')), 'fg', 'gui')
let s:truebgr = synIDattr(synIDtrans(hlID('Normal')), 'bg', 'gui')
let s:truefgr = empty('s:truefgr') ? '#E1E1E1' : s:truefgr
let s:truebgr = empty('s:truebgr') ? '#111111' : s:truebgr

let g:status_colors = { 'backgr': [s:truebgr, 0], 'foregr': [s:truefgr, 7] }
